(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/SearchPipe.ts":
/*!*******************************!*\
  !*** ./src/app/SearchPipe.ts ***!
  \*******************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, field, value) {
        if (!items) {
            return [];
        }
        if (!field || !value) {
            return items;
        }
        return items.filter(function (singleItem) {
            return singleItem[field].toLowerCase().includes(value.toLowerCase());
        });
    };
    FilterPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'filter'
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], FilterPipe);
    return FilterPipe;
}());



/***/ }),

/***/ "./src/app/addupdatetask/addupdatetask.component.css":
/*!***********************************************************!*\
  !*** ./src/app/addupdatetask/addupdatetask.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".textboxwidth{\r\n    width:370px;\r\n    font-size: 12px;\r\n    background-color:#E8E8E8;\r\n}\r\n\r\n.labelstyle{\r\n    font-weight: 15px;\r\n    font-size: 15px; \r\n    }\r\n\r\n.buttonwidth{\r\n        width:75px; \r\n        font-size: 12px;\r\n        font-weight: bold; \r\n        /* margin-top: 25px; */\r\n    }"

/***/ }),

/***/ "./src/app/addupdatetask/addupdatetask.component.html":
/*!************************************************************!*\
  !*** ./src/app/addupdatetask/addupdatetask.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "   \n<br/>\n<div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            <label class=\"labelstyle\">Project:</label>\n            </div>\n            <div class=\"col-lg-5 col-md-5 col-sm-5\">\n            <input #Project [(ngModel)]=\"this.getProject.Project_Name\"  disabled='true'  class=\"textboxwidth\" type=\"text\" /> &nbsp;  \n            <button id=\"SearchBtn\" class=\"buttonwidth\" data-toggle=\"modal\" data-target=\"#projectModal\" >Search</button>         \n          </div>  \n                \n  </div>\n  </div>\n  <br/>\n<div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            <label class=\"labelstyle\">Task:</label>\n            </div>\n            <div class=\"col-lg-5 col-md-5 col-sm-5\">\n            <input [(ngModel)]=\"taskDetail.TaskName\"   class=\"textboxwidth\" type=\"text\" />           \n          </div>        \n  </div>\n  </div>\n  <br/>\n\n  <div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n\n            </div>               \n            <div class=\"col-lg-5 col-md-5 col-sm-5\">               \n                <input type=\"checkbox\" #Parenttask name=\"Parenttask\" id=\"Parenttask\" (click)=\"SetParenttask(Parenttask)\" value=\"\"> Parent Task                 \n            </div>\n    </div>\n</div>\n\n<br/>\n\n  \n  <div class=\"row\">\n      <div class=\"col-lg-12 col-md-12\">\n          <div class=\"col-lg-1 col-md-1 col-sm-1\">\n              <label class=\"labelstyle\">Priority:</label> \n              </div>               \n              <div class=\"col-lg-5 col-md-5 col-sm-5\">               \n              <input [(ngModel)]=\"taskDetail.Priority\" class=\"textboxwidth\" [disabled]=\"accesslevel\" type=\"range\" name=\"points\" min=\"0\" max=\"30\">                  \n    </div>\n  </div>\n</div>\n\n  <br/><br/>\n  <div class=\"row\">\n      <div class=\"col-lg-12 col-md-12\">\n          <div class=\"col-lg-1 col-md-1 col-sm-1\">\n              <label class=\"labelstyle\">Parent Task:</label>\n              </div>\n              <div class=\"col-lg-5 col-md-5 col-sm-5\">\n              <input [(ngModel)]=\"this.getparentTask.Parent_Task\" [disabled]=\"accesslevel\" class=\"textboxwidth\" disabled ='true'  />  &nbsp;\n              <button id=\"SearchBtnprojecttaskModal\" [disabled]=\"accesslevel\" class=\"buttonwidth\" data-toggle=\"modal\" data-target=\"#projecttaskModal\" >Search</button>\n            </div>        \n    </div>\n  </div>\n\n  <br/><br/>\n  <div class=\"row\">\n      <div class=\"col-lg-12 col-md-12\">\n          <div class=\"col-lg-1 col-md-1 col-sm-1\">\n              <label class=\"labelstyle\">Start Date:</label>\n              </div>\n              <div class=\"col-lg-3 col-md-3 col-sm-3\">   \n              <input id=\"startdate\" [disabled]=\"accesslevel\"  [(ngModel)]=\"taskDetail.StartDate\" value=\"taskDetail.StartDate\" (ngModelChange)=\"taskDetail.StartDate = $event\"  class=\"textboxwidth\" type=\"date\" />\n            </div>  \n            \n            <div class=\"col-lg-1 col-md-1 col-sm-1\">\n                <label class=\"labelstyle\">End Date:</label>\n                </div>\n                <div class=\"col-lg-5 col-md-5 col-sm-5\"> \n                <input id=\"endtdate\" [disabled]=\"accesslevel\" [(ngModel)]=\"taskDetail.EndDate\" value=\"taskDetail.EndDate\" (ngModelChange)=\"taskDetail.EndDate = $event\" class=\"textboxwidth\" type=\"date\" />\n              </div>  \n    </div>\n  </div>\n\n  \n  <!-- <div class=\"row\">\n      <div class=\"col-lg-12 col-md-12\">\n          <div class=\"col-lg-1 col-md-1 col-sm-1\">\n              <label class=\"labelstyle\">End Date:</label>\n              </div>\n              <div class=\"col-lg-5 col-md-5 col-sm-5\"> \n              <input id=\"endtdate\" [(ngModel)]=\"taskDetail.EndDate\" value=\"taskDetail.EndDate\" (ngModelChange)=\"taskDetail.EndDate = $event\" class=\"textboxwidth\" type=\"date\" />\n            </div>        \n    </div>\n  </div> -->\n  <br/><br/>\n  <div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            <label class=\"labelstyle\">User:</label>\n            </div>\n            <div class=\"col-lg-5 col-md-5 col-sm-5\">\n            <input [(ngModel)]=\"this.userDetail.First_Name\" disabled='true'  class=\"textboxwidth\" type=\"text\" /> &nbsp;  \n            <button id=\"SearchBtn\" [disabled]=\"accesslevel\" class=\"buttonwidth\" data-toggle=\"modal\" data-target=\"#userModal\" >Search</button>         \n          </div>  \n                \n  </div>\n  </div>\n\n  <br/><br/>\n\n  <div class=\"row\">\n      <div class=\"col-lg-12 col-md-12\">\n          <div class=\"col-lg-1 col-md-1 col-sm-1\">\n               \n              </div>\n              <div class=\"col-lg-5 col-md-5 col-sm-5\">\n                  <button id=\"submitBtn\" class=\"buttonwidth\" (click)=\"onSubmit(Parenttask)\">Add Task</button>&nbsp;\n                  <button id=\"clearBtn\" class=\"buttonwidth\" (click)=\"onClear(Parenttask)\">Reset</button>\n            </div>        \n    </div>\n  </div>\n\n  <div class=\"modal fade\" id=\"projectModal\" #projectModal tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Project</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          \n            <div class=\"row\">\n                <hr style=\"border: 1px dotted gainsboro;\" />\n                <div class=\"col-lg-12 col-md-12\">\n                \n                    <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label class=\"labelstyle\">Search by Name:</label>\n                        </div>\n                        <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                        <input  [(ngModel)]=\"searchString\"   class=\"textboxwidth\" type=\"text\" />           \n                      </div>\n                      \n                </div>\n            </div>\n          \n                      \n            <br/><br/>\n            <div class=\"col-lg-12 col-md-12 col-sm-12\">\n            <ul class=\"list-group\" *ngFor=\"let project of allProjectList |filter : 'Project_Name' : searchString; let i = index\">\n               \n                  <div class=\"row\">\n                      <hr style=\"border: 1px dotted gainsboro;\" />\n                  <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label > Project Name: </label>\n                    <label >{{project.Project_Name}}</label> <br/> \n                    <label > Start Date: </label>\n                    <label >{{project.Start_Date}}</label> <br/>\n                    <label > End_Date: </label>\n                    <label >{{project.End_Date}}</label><br/>\n                  </div>\n                  <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                    <button #selectButton class=\"buttonwidth\"  id=\"selectButton\" (click)=\"selectproject(project,projectModal)\" data-dismiss=\"modal\">Select</button> <br/><br/>\n                            \n                  </div>\n                </div>\n                                \n              </ul>\n            </div>          \n        </div>\n        <div class=\"modal-footer\">\n          <!-- <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n        </div>\n      </div>\n    </div>\n  </div> <!--End of Project-->\n\n\n  <!-- user  Modal -->\n  <div class=\"modal fade\" id=\"userModal\" #userModal tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Users</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          \n            <div class=\"row\">\n                <hr style=\"border: 1px dotted gainsboro;\" />\n                <div class=\"col-lg-12 col-md-12\">\n                \n                    <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label class=\"labelstyle\">Search by First Name:</label>\n                        </div>\n                        <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                        <input  [(ngModel)]=\"searchString\"   class=\"textboxwidth\" type=\"text\" />           \n                      </div>\n                      \n                </div>\n            </div>\n          \n                      \n            <br/><br/>\n            <div class=\"col-lg-12 col-md-12 col-sm-12\">\n            <ul class=\"list-group\" *ngFor=\"let user of allUserList |filter : 'First_Name' : searchString; let i = index\">\n               \n                  <div class=\"row\">\n                      <hr style=\"border: 1px dotted gainsboro;\" />\n                  <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label class=\"labelstyle\"> First Name: </label>\n                    <label class=\"labeltextarea\">{{user.First_Name}}</label> <br/> \n                    <label class=\"labelstyle\"> Last Name: </label>\n                    <label class=\"labeltextarea\">{{user.Last_Name}}</label> <br/>\n                    <label class=\"labelstyle\"> EmployeeID: </label>\n                    <label class=\"labeltextarea\">{{user.Employee_ID}}</label><br/>\n                  </div>\n                  <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                    <button #selectButton class=\"buttonwidth\"  id=\"selectButton\" (click)=\"selectUser(user,userModal)\" data-dismiss=\"modal\">Select</button> <br/><br/>\n                            \n                  </div>\n                </div>\n                                \n              </ul>\n            </div>\n          \n\n        </div>\n        <div class=\"modal-footer\">\n          <!-- <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n  <!-- user  Modal -->\n  <div class=\"modal fade\" id=\"projecttaskModal\" #projecttaskModal tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">ProjectTask</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          \n            <div class=\"row\">\n                <hr style=\"border: 1px dotted gainsboro;\" />\n                <div class=\"col-lg-12 col-md-12\">\n                \n                    <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label class=\"labelstyle\">Search by Parent task:</label>\n                        </div>\n                        <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                        <input  [(ngModel)]=\"searchString\"   class=\"textboxwidth\" type=\"text\" />           \n                      </div>\n                      \n                </div>\n            </div>\n          \n                      \n            <br/><br/>\n            <div class=\"col-lg-12 col-md-12 col-sm-12\">\n            <ul class=\"list-group\" *ngFor=\"let parentTask of allParenttask |filter : 'Parent_Task' : searchString; let i = index\">\n               \n                  <div class=\"row\">\n                      <hr style=\"border: 1px dotted gainsboro;\" />\n                  <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label class=\"labelstyle\"> Parent Task: </label>\n                    <label class=\"labeltextarea\">{{parentTask.Parent_Task}}</label> <br/>                    \n                  </div>\n                  <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                    <button #selectButton class=\"buttonwidth\"  id=\"selectButton\" (click)=\"selectparentTask(parentTask,projecttaskModal)\" data-dismiss=\"modal\" >Select</button> <br/><br/>\n                            \n                  </div>\n                </div>\n                                \n              </ul>\n            </div>\n          \n        </div>\n        <div class=\"modal-footer\">\n          <!-- <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "./src/app/addupdatetask/addupdatetask.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/addupdatetask/addupdatetask.component.ts ***!
  \**********************************************************/
/*! exports provided: AddupdatetaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddupdatetaskComponent", function() { return AddupdatetaskComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_taskservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/taskservice.service */ "./src/app/services/taskservice.service.ts");
/* harmony import */ var _models_taskmodel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/taskmodel */ "./src/app/models/taskmodel.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _models_projectModel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/projectModel */ "./src/app/models/projectModel.ts");
/* harmony import */ var _models_usermodel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/usermodel */ "./src/app/models/usermodel.ts");
/* harmony import */ var _models_parentTaskModel__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/parentTaskModel */ "./src/app/models/parentTaskModel.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AddupdatetaskComponent = /** @class */ (function () {
    function AddupdatetaskComponent(taskService, route, router, datePipe) {
        this.taskService = taskService;
        this.route = route;
        this.router = router;
        this.datePipe = datePipe;
        this.mode = "Add";
        this.disabled = false;
        this.allProjectList = [];
        this.allParenttask = [];
        this.accesslevel = false;
    }
    AddupdatetaskComponent.prototype.ngOnInit = function () {
        this.taskID = this.route.snapshot.paramMap.get('id');
        this.taskDetail = new _models_taskmodel__WEBPACK_IMPORTED_MODULE_3__["taskModel"]();
        this.getProject = new _models_projectModel__WEBPACK_IMPORTED_MODULE_5__["projectModel"]();
        this.userDetail = new _models_usermodel__WEBPACK_IMPORTED_MODULE_6__["userModel"]();
        this.getparentTask = new _models_parentTaskModel__WEBPACK_IMPORTED_MODULE_7__["parentTaskModel"]();
        this.allUserList = [];
        debugger;
        if (this.mode == "Add") {
            this.SelectDate();
            this.getAllProjectDetails();
            this.getAlluser();
            this.getAllParentTask();
        }
        if (this.taskID) {
            var addTask = document.getElementById("updateAnchor");
            var viewTask = document.getElementById("viewAnchor");
            var submitButton = document.getElementById("submitBtn");
            var cancelButton = document.getElementById("clearBtn");
            //this.SelectDate();
            addTask.innerHTML = "Update Task";
            //viewTask.innerHTML="";
            submitButton.innerHTML = "Update";
            cancelButton.innerHTML = "Cancel";
            this.mode = "Edit";
            this.getTaskByID();
        }
    };
    AddupdatetaskComponent.prototype.getAllProjectDetails = function () {
        var _this = this;
        this.taskService.getAllProjectDetails().subscribe(function (getProject) {
            _this.allProjectList = getProject;
            //this.getProject 
            if (_this.taskDetail.Project_ID != null)
                _this.getProject = _this.allProjectList.filter(function (A) { return A.Project_ID == _this.taskDetail.Project_ID; })[0];
        });
    };
    AddupdatetaskComponent.prototype.getAllParentTask = function () {
        var _this = this;
        this.taskService.getAllParentTasks().subscribe(function (getParenttask) {
            _this.allParenttask = getParenttask;
            if (_this.taskDetail.Parent_ID != null)
                _this.getparentTask = _this.allParenttask.filter(function (A) { return A.Parent_ID == _this.taskDetail.Parent_ID; })[0];
        });
    };
    AddupdatetaskComponent.prototype.getAlluser = function () {
        var _this = this;
        this.taskService.getAllUsers().subscribe(function (getuser) {
            debugger;
            _this.allUserList = getuser;
            if (_this.taskDetail.User_ID != null)
                _this.userDetail = _this.allUserList.filter(function (A) { return A.User_ID == _this.taskDetail.User_ID; })[0];
        });
    };
    AddupdatetaskComponent.prototype.getTaskByID = function () {
        var _this = this;
        this.taskService.getAllTasksByID(this.taskID)
            .subscribe(function (result) {
            _this.taskDetail = result;
            _this.taskDetail.StartDate = _this.datePipe.transform(_this.taskDetail.StartDate, 'yyyy-MM-dd');
            _this.taskDetail.EndDate = _this.datePipe.transform(_this.taskDetail.EndDate, 'yyyy-MM-dd');
            if (_this.taskDetail.Status == "Y") {
                //this.disabled=true;
                _this.taskDetail.StartDate = null;
                _this.taskDetail.EndDate = null;
            }
            else {
                _this.disabled = false;
                _this.taskDetail.StartDate = _this.datePipe.transform(_this.taskDetail.StartDate, 'yyyy-MM-dd');
                _this.taskDetail.EndDate = _this.datePipe.transform(_this.taskDetail.EndDate, 'yyyy-MM-dd');
            }
        }, function (error) { }, function () {
            _this.getAllProjectDetails();
            _this.getAlluser();
            _this.getAllParentTask();
        });
    };
    AddupdatetaskComponent.prototype.onSubmit = function (parenttask) {
        var _this = this;
        if (!this.validateMandatoryData(parenttask)) {
            if (parenttask.checked == true) {
                alert('Please poulate Task Name & project Name ');
                return;
            }
            if (parenttask.checked == false) {
                alert('Please poulate all fields except Parent Task');
                return;
            }
        }
        if (parenttask.checked == false) {
            if (!this.validateDateCompare(parenttask)) {
                alert('End Date should not be before Start date');
                return;
            }
        }
        if (parenttask.checked == true) {
            this.taskDetail.IsParent_Task = "Y";
        }
        else {
            this.taskDetail.IsParent_Task = "N";
        }
        this.taskDetail.Parent_ID = this.getparentTask.Parent_ID;
        this.taskDetail.Project_ID = this.getProject.Project_ID;
        this.taskDetail.User_ID = this.userDetail.User_ID;
        if (this.mode == "Edit") {
            this.taskService.updateTask(this.taskDetail)
                .subscribe(function (result) {
                _this.router.navigateByUrl('/ViewTaskManager');
            });
        }
        else {
            this.taskService.addTask(this.taskDetail)
                .subscribe(function (result) {
                _this.router.navigateByUrl('/ViewTaskManager');
            });
        }
    };
    AddupdatetaskComponent.prototype.validateDateCompare = function (parentTask) {
        if (parentTask.checked == false) {
            if (new Date(this.taskDetail.StartDate) > new Date(this.taskDetail.EndDate)) {
                return false;
            }
            //return false;
        }
        return true;
    };
    AddupdatetaskComponent.prototype.SelectDate = function () {
        // this.disabledControl =false;
        var date = new Date();
        this.taskDetail.StartDate = this.datePipe.transform(date, 'yyyy-MM-dd');
        this.taskDetail.EndDate = this.datePipe.transform(this.addDays(date, 1), 'yyyy-MM-dd');
    };
    AddupdatetaskComponent.prototype.SetParenttask = function (parenttask) {
        if (parenttask.checked == true) {
            this.accesslevel = true;
            this.taskDetail.StartDate = null;
            this.taskDetail.EndDate = null;
        }
        else {
            this.accesslevel = false;
            this.SelectDate();
        }
    };
    AddupdatetaskComponent.prototype.addDays = function (date, days) {
        date.setDate(date.getDate() + days);
        return date;
    };
    AddupdatetaskComponent.prototype.onClear = function (parenttask) {
        if (parenttask.checked == true) {
        }
        if (this.mode == "Edit") {
            this.router.navigateByUrl('/ViewTaskManager');
        }
        else {
            this.taskDetail = new _models_taskmodel__WEBPACK_IMPORTED_MODULE_3__["taskModel"]();
        }
    };
    AddupdatetaskComponent.prototype.onDateSelect = function (value) {
        var startdate = document.getElementById("startdate");
        this.taskDetail.StartDate = startdate.innerText;
    };
    AddupdatetaskComponent.prototype.selectproject = function (project, projectModal) {
        this.getProject = project;
    };
    AddupdatetaskComponent.prototype.selectparentTask = function (projectTask, projectTaskModal) {
        this.getparentTask = projectTask;
    };
    AddupdatetaskComponent.prototype.selectUser = function (user, userModal) {
        this.userDetail = user;
    };
    AddupdatetaskComponent.prototype.validateMandatoryData = function (parentTask) {
        debugger;
        if (parentTask.checked == true) {
            if (this.getProject.Project_Name && this.taskDetail.TaskName) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if (this.getProject.Project_Name && this.userDetail.User_ID && this.taskDetail.TaskName) {
                return true;
            }
        }
        return false;
    };
    AddupdatetaskComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-addupdatetask',
            template: __webpack_require__(/*! ./addupdatetask.component.html */ "./src/app/addupdatetask/addupdatetask.component.html"),
            styles: [__webpack_require__(/*! ./addupdatetask.component.css */ "./src/app/addupdatetask/addupdatetask.component.css")],
            providers: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]]
        }),
        __metadata("design:paramtypes", [_services_taskservice_service__WEBPACK_IMPORTED_MODULE_2__["TaskserviceService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]])
    ], AddupdatetaskComponent);
    return AddupdatetaskComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _viewtask_viewtask_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./viewtask/viewtask.component */ "./src/app/viewtask/viewtask.component.ts");
/* harmony import */ var _addupdatetask_addupdatetask_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./addupdatetask/addupdatetask.component */ "./src/app/addupdatetask/addupdatetask.component.ts");
/* harmony import */ var _project_project_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./project/project.component */ "./src/app/project/project.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: '', component: _viewtask_viewtask_component__WEBPACK_IMPORTED_MODULE_2__["ViewtaskComponent"] },
    { path: 'ViewTaskManager', component: _viewtask_viewtask_component__WEBPACK_IMPORTED_MODULE_2__["ViewtaskComponent"] },
    { path: 'Addupdatetask', component: _addupdatetask_addupdatetask_component__WEBPACK_IMPORTED_MODULE_3__["AddupdatetaskComponent"] },
    { path: 'Addupdatetask/:id', component: _addupdatetask_addupdatetask_component__WEBPACK_IMPORTED_MODULE_3__["AddupdatetaskComponent"] },
    { path: 'project', component: _project_project_component__WEBPACK_IMPORTED_MODULE_4__["ProjectComponent"] },
    { path: 'user', component: _user_user_component__WEBPACK_IMPORTED_MODULE_5__["UserComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mainheading{\r\n    font-weight: 20px;\r\n    font-size: 25px; \r\n    }\r\n    .taskheading{\r\n        font-weight: 20px;\r\n        font-size: 18px; \r\n    }"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " \n<br>\n<br>\n  <div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n      <div class=\"col-lg-3 col-md-3 mainheading\"><B>Project Manager</B></div>\n        \n  </div>\n      \n  </div>\n  <br>\n  <br>\n  <div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">  \n         \n   <div class=\"col-lg-2 col-md-2 taskheading\"><a id=\"project\" routerLink=\"/project\"><B>Add Project</B></a></div>  \n   <div class=\"col-lg-2 col-md-2 taskheading\"><a id=\"updateAnchor\" routerLink=\"/Addupdatetask\"><B>Add Task</B></a></div>  \n   <div class=\"col-lg-2 col-md-2 taskheading\"><a id=\"user\" routerLink=\"/user\"><B>Add User</B></a></div>    \n   <div class=\"col-lg-2 col-md-2 taskheading\"><a id=\"viewAnchor\" routerLink=\"/ViewTaskManager\"><B>View Task</B></a></div>\n  </div>\n</div> \n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Project Manager';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _viewtask_viewtask_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./viewtask/viewtask.component */ "./src/app/viewtask/viewtask.component.ts");
/* harmony import */ var _addupdatetask_addupdatetask_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./addupdatetask/addupdatetask.component */ "./src/app/addupdatetask/addupdatetask.component.ts");
/* harmony import */ var _project_project_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./project/project.component */ "./src/app/project/project.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _SearchPipe__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./SearchPipe */ "./src/app/SearchPipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _viewtask_viewtask_component__WEBPACK_IMPORTED_MODULE_6__["ViewtaskComponent"],
                _addupdatetask_addupdatetask_component__WEBPACK_IMPORTED_MODULE_7__["AddupdatetaskComponent"],
                _project_project_component__WEBPACK_IMPORTED_MODULE_8__["ProjectComponent"],
                _user_user_component__WEBPACK_IMPORTED_MODULE_9__["UserComponent"],
                _SearchPipe__WEBPACK_IMPORTED_MODULE_10__["FilterPipe"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/constants.ts":
/*!******************************!*\
  !*** ./src/app/constants.ts ***!
  \******************************/
/*! exports provided: AppConstants */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppConstants", function() { return AppConstants; });
var AppConstants = /** @class */ (function () {
    function AppConstants() {
        //public baseUrl:string="http://localhost:51623/";
        this.baseUrl = "http://localhost/ProjectManager.API/";
        this.getTasks = "GetAllTasks";
        this.getTaskByID = "GetTaskByID";
        this.addTask = "PostTask";
        this.updateTask = "UpdateTask";
        this.addUser = "PostUser";
        this.updateUser = "UpdateUser";
        this.getAllUsers = 'GetAllUsers';
        this.deleteUser = 'DeleteUser';
        this.postProject = 'PostProject';
        this.getAllProjectDetails = 'GetAllProjectDetails';
        this.updateProject = "UpdateProject";
        this.deleteProject = "DeleteProject";
        this.getAllParentTasks = 'GetAllParentTasks';
    }
    return AppConstants;
}());



/***/ }),

/***/ "./src/app/models/parentTaskModel.ts":
/*!*******************************************!*\
  !*** ./src/app/models/parentTaskModel.ts ***!
  \*******************************************/
/*! exports provided: parentTaskModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parentTaskModel", function() { return parentTaskModel; });
var parentTaskModel = /** @class */ (function () {
    function parentTaskModel() {
    }
    return parentTaskModel;
}());



/***/ }),

/***/ "./src/app/models/projectModel.ts":
/*!****************************************!*\
  !*** ./src/app/models/projectModel.ts ***!
  \****************************************/
/*! exports provided: projectModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "projectModel", function() { return projectModel; });
//Project_ID
var projectModel = /** @class */ (function () {
    function projectModel() {
    }
    return projectModel;
}());



/***/ }),

/***/ "./src/app/models/taskmodel.ts":
/*!*************************************!*\
  !*** ./src/app/models/taskmodel.ts ***!
  \*************************************/
/*! exports provided: taskModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "taskModel", function() { return taskModel; });
var taskModel = /** @class */ (function () {
    function taskModel() {
        this.DisabledMode = false;
        this.Priority = "0";
    }
    return taskModel;
}());



/***/ }),

/***/ "./src/app/models/userModel.ts":
/*!*************************************!*\
  !*** ./src/app/models/userModel.ts ***!
  \*************************************/
/*! exports provided: userModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userModel", function() { return userModel; });
var userModel = /** @class */ (function () {
    function userModel() {
        this.User_ID = '';
        this.First_Name = '';
        this.Last_Name = '';
        this.Employee_ID = '';
    }
    return userModel;
}());



/***/ }),

/***/ "./src/app/models/usermodel.ts":
/*!*************************************!*\
  !*** ./src/app/models/usermodel.ts ***!
  \*************************************/
/*! exports provided: userModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userModel", function() { return userModel; });
var userModel = /** @class */ (function () {
    function userModel() {
        this.User_ID = '';
        this.First_Name = '';
        this.Last_Name = '';
        this.Employee_ID = '';
    }
    return userModel;
}());



/***/ }),

/***/ "./src/app/project/project.component.css":
/*!***********************************************!*\
  !*** ./src/app/project/project.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/project/project.component.html":
/*!************************************************!*\
  !*** ./src/app/project/project.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br/><br/>\n\n<div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            <label class=\"labelstyle\">Project:</label>\n            </div>\n            <div class=\"col-lg-5 col-md-5 col-sm-5\">\n            <input [(ngModel)]=\"projectDetail.Project_Name\" required   class=\"textboxwidth\" type=\"text\" />           \n            \n                  \n        </div> \n               \n  </div>\n  </div>\n  <br/>\n\n  \n<div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            <input type=\"checkbox\" #StartandEndDate name=\"StartandEndDate\" (click)=\"SelectDate(StartandEndDate)\" value=\"\"> <B>Set Start & End date </B>\n            </div>\n            \n                <div class=\"col-lg-1 col-md-1 col-sm-1\">\n                    <label class=\"labelstyle\">Start Date:</label>\n                   </div>\n                   <div class=\"col-lg-3 col-md-3 col-sm-3\">                   \n\n                    <input id=\"StartDate\" [disabled]=\"disabledControl\" [(ngModel)]=\"projectDetail.Start_Date\" value=\"projectDetail.Start_Date\" (ngModelChange)=\"projectDetail.Start_Date = $event\" class=\"textboxwidth\" type=\"date\" />\n                </div>\n\n                <div class=\"col-lg-1 col-md-1 col-sm-1\">\n                    <label class=\"labelstyle\">&nbsp;End Date:</label>\n                   </div>\n                   <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                    <!-- <input #EndDate class=\"datetextbox\"  type=\"date\" /> -->\n                    <input id=\"EndDate\" [disabled]=\"disabledControl\" placeholder=\"End Date\" [(ngModel)]=\"projectDetail.End_Date\" value=\"projectDetail.End_Date\" (ngModelChange)=\"projectDetail.End_Date = $event\" class=\"textboxwidth\" type=\"date\" />\n                </div>\n                     \n               \n  </div>\n  </div>\n  <br/>\n\n  <div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            <label class=\"labelstyle\">Priority:</label>\n            </div>               \n            <div class=\"col-lg-5 col-md-5 col-sm-5\"> \n                    <input [(ngModel)]=\"projectDetail.Priority\" class=\"textboxwidth\" type=\"range\" name=\"points\" min=\"0\" max=\"30\">              \n            <!-- <input class=\"textboxwidth\" type=\"range\" name=\"points\" min=\"0\" max=\"30\">                   -->\n  </div>\n</div>\n</div>\n<br/>\n<div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            <label class=\"labelstyle\">Manager:</label>\n            </div>\n            <div class=\"col-lg-2 col-md-2 col-sm-2\">\n            <!-- <input  [(ngModel)] =\"this.userDetail.First_Name\"  class=\"textboxwidth\" type=\"text\" />  -->\n            <!-- <label class=\"labelstyle\">{{this.userDetail.First_Name}} {{this.userDetail.Last_Name}}</label>   -->\n            <input  [(ngModel)]=\"this.userDetail.First_Name\"  disabled='true'  class=\"textboxwidth\" type=\"text\" />            \n          </div> \n          \n          <div class=\"col-lg-2 col-md-2 col-sm-2\">\n            <button id=\"SearchBtn\" class=\"buttonwidth\" data-toggle=\"modal\" data-target=\"#userModal\" >Search</button>  \n          </div>\n\n          \n  </div>\n  </div>\n  <br/>\n  <br/>\n  <div class=\"row\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            </div> \n  <div class=\"col-lg-5 col-md-5 col-sm-5\">\n    <button #submitBtn id=\"submitBtn\" class=\"buttonwidth\" (click)=\"onSubmit(StartandEndDate)\">Add </button>&nbsp;\n    <button id=\"clearBtn\" class=\"buttonwidth\" (click)=\"onClear(StartandEndDate)\">Reset</button>\n</div>  \n</div>\n\n\n\n<br/>\n<div class=\"row\">\n        <hr style=\"border: 1px solid gainsboro;\" />\n</div>\n<div class=\"row\">\n<div class=\"col-lg-2 col-md-2 col-sm-2\">\n        <label >&nbsp;&nbsp;&nbsp;&nbsp;Search:</label>\n        </div>\n        <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                &nbsp; <input  [(ngModel)]=\"searchString\"   class=\"textboxwidth\" type=\"text\" />           \n      </div> \n    </div>\n    <br/>\n    <div>\n            <div class=\"col-lg-2 col-md-2 col-sm-2\">\n                    <label >Sort:</label>\n                    </div>\n    \n                    <div class=\"col-lg-3 col-md3 col-sm-3\">\n                        <button  (click)=\"sortByDate()\" >Start Date:</button>&nbsp;\n                        <button (click)=\"sortByEnddate()\" >End Date:</button>&nbsp;\n                        <button (click)=\"sortByPriority()\"  >Priority</button>&nbsp;\n                        <button (click)=\"sortByCompleted()\"  >Completed</button>&nbsp;\n                        </div>\n    </div>\n\n   \n  <div class=\"col-lg-12 col-md-12 col-sm-12\">\n  <ul class=\"list-group\" *ngFor=\"let project of allProjectList  | filter : 'Project_Name' : searchString; let i = index\">\n      <!-- <li class=\"list-group-item d-flex justify-content-between align-items-center\"> -->\n        <div class=\"row\">\n            <hr style=\"border: 1px dotted gainsboro;\" />\n            \n        <div class=\"col-lg-4 col-md-4 col-sm-4\">\n            Project:\n          <label class=\"labeltextarea\">{{project.Project_Name}}</label> <br/> \n            No of Tasks:\n          <label class=\"labeltextarea\">{{project.NoOfTasks==null?0:project.NoOfTasks}}</label>\n          No Of Tasks Completed:\n          \n          <label class=\"labeltextarea\">{{project.NoOfTasksCompleted==null?0:project.NoOfTasksCompleted}}</label><br/>\n\n          Start Date:\n          <label class=\"labeltextarea\">{{project.Start_Date | date:'dd/MM/yyy'}}</label> \n\n          End Date:\n          <label class=\"labeltextarea\">{{project.End_Date | date:'dd/MM/yyy'}}</label>\n          <!-- <label class=\"labeltextarea\">{{project.NoOfTasks}}</label><br/> -->\n          <!-- <label class=\"labeltextarea\">{{project.NoOfTasksCompleted}}</label><br/> -->\n        </div>\n        <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                Priority:\n            <label class=\"labeltextarea\">{{project.Priority}}</label>\n        </div>\n        <div class=\"col-lg-3 col-md-3 col-sm-3\">\n          <button #editButton class=\"buttonwidth\"  id=\"editButton\" (click)=\"editProject(project,StartandEndDate)\" >Update</button> <br/><br/>\n                   <button #deleteButton id=\"deleteButton\"  (click)=\"deleteProject(project)\">Suspend</button> \n        </div>\n      </div>\n      <!-- </li> -->\n      \n    </ul>\n  </div>\n\n  \n\n\n  <!-- Button trigger modal -->\n \n  <!-- user  Modal -->\n  <div class=\"modal fade\" id=\"userModal\" #userModal tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Users</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          \n            <div class=\"row\">\n                <hr style=\"border: 1px dotted gainsboro;\" />\n                <div class=\"col-lg-12 col-md-12\">\n                \n                    <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label class=\"labelstyle\">Search by First Name:</label>\n                        </div>\n                        <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                        <input  [(ngModel)]=\"searchString\"   class=\"textboxwidth\" type=\"text\" />           \n                      </div>\n                      \n                </div>\n            </div>\n          \n                      \n            <br/><br/>\n            <div class=\"col-lg-12 col-md-12 col-sm-12\">\n            <ul class=\"list-group\" *ngFor=\"let user of allUserList |filter : 'First_Name' : searchString; let i = index\">\n               \n                  <div class=\"row\">\n                      <hr style=\"border: 1px dotted gainsboro;\" />\n                  <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label class=\"labelstyle\"> First Name: </label>\n                    <label class=\"labeltextarea\">{{user.First_Name}}</label> <br/> \n                    <label class=\"labelstyle\"> Last Name: </label>\n                    <label class=\"labeltextarea\">{{user.Last_Name}}</label> <br/>\n                    <label class=\"labelstyle\"> EmployeeID: </label>\n                    <label class=\"labeltextarea\">{{user.Employee_ID}}</label><br/>\n                  </div>\n                  <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                    <button #selectButton class=\"buttonwidth\"  id=\"selectButton\" (click)=\"selectUser(user,userModal)\"  data-dismiss=\"modal\" >Select</button> <br/><br/>\n                            \n                  </div>\n                </div>\n                                \n              </ul>\n            </div>\n          \n\n\n\n        </div>\n        <div class=\"modal-footer\">\n          <!-- <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n        </div>\n      </div>\n    </div>\n  </div>\n\n"

/***/ }),

/***/ "./src/app/project/project.component.ts":
/*!**********************************************!*\
  !*** ./src/app/project/project.component.ts ***!
  \**********************************************/
/*! exports provided: ProjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectComponent", function() { return ProjectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_usermodel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/usermodel */ "./src/app/models/usermodel.ts");
/* harmony import */ var _services_taskservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/taskservice.service */ "./src/app/services/taskservice.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _models_projectModel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/projectModel */ "./src/app/models/projectModel.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_6__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProjectComponent = /** @class */ (function () {
    function ProjectComponent(taskService, route, router, datePipe) {
        this.taskService = taskService;
        this.route = route;
        this.router = router;
        this.datePipe = datePipe;
        this.mode = 'Add';
        this.disabledControl = true;
    }
    ProjectComponent.prototype.ngOnInit = function () {
        this.userDetail = new _models_usermodel__WEBPACK_IMPORTED_MODULE_1__["userModel"]();
        this.projectDetail = new _models_projectModel__WEBPACK_IMPORTED_MODULE_5__["projectModel"]();
        this.allUserList = [];
        this.getAlluser();
        this.getAllProjectDetails();
    };
    ProjectComponent.prototype.getAlluser = function () {
        var _this = this;
        this.taskService.getAllUsers().subscribe(function (getuser) {
            _this.allUserList = getuser;
        });
    };
    ProjectComponent.prototype.getAllProjectDetails = function () {
        var _this = this;
        this.taskService.getAllProjectDetails().subscribe(function (getProject) {
            _this.allProjectList = getProject;
        });
    };
    ProjectComponent.prototype.selectUser = function (user, userModal) {
        this.userDetail = user;
        //userModal.modal('hide')
        jquery__WEBPACK_IMPORTED_MODULE_6__('#userModal').modal('hide');
        //document.getElementById('userModal').hidden = true;
    };
    ProjectComponent.prototype.SelectDate = function (startandEndDate) {
        //alert(this.projectDetail.End_Date);
        if (startandEndDate.checked == true) {
            this.disabledControl = false;
            var date = new Date();
            this.projectDetail.Start_Date = this.datePipe.transform(date, 'yyyy-MM-dd');
            this.projectDetail.End_Date = this.datePipe.transform(this.addDays(date, 1), 'yyyy-MM-dd');
        }
        else {
            this.projectDetail.Start_Date = null;
            this.projectDetail.End_Date = null;
            this.disabledControl = true;
        }
    };
    ProjectComponent.prototype.addDays = function (date, days) {
        date.setDate(date.getDate() + days);
        return date;
    };
    ProjectComponent.prototype.validateDateCompare = function (startandEndDate) {
        if (startandEndDate.checked == true) {
            if (new Date(this.projectDetail.Start_Date) > new Date(this.projectDetail.End_Date)) {
                return false;
            }
            //return false;
        }
        return true;
    };
    ProjectComponent.prototype.validateDate = function (startandEndDate) {
        if (startandEndDate.checked == true) {
            if (this.projectDetail.Start_Date && this.projectDetail.End_Date) {
                if (this.projectDetail.Project_Name && this.userDetail.User_ID) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        else {
            if (this.projectDetail.Project_Name && this.projectDetail.User_ID) {
                return true;
            }
        }
        return false;
    };
    ProjectComponent.prototype.onSubmit = function (startandEndDate) {
        var _this = this;
        //this.projectDetail
        debugger;
        if (!this.validateDate(startandEndDate)) {
            alert('Please populate all fields');
            return;
        }
        if (!this.validateDateCompare(startandEndDate)) {
            alert('End Date should not be before Start date');
            return;
        }
        if (this.mode == 'Add') {
            this.projectDetail.User_ID = this.userDetail.User_ID;
            this.taskService.PostProject(this.projectDetail).subscribe(function (A) { _this.getAllProjectDetails; }, function (error) { }, function () {
                _this.getAllProjectDetails();
            });
        }
        else if (this.mode == 'Edit') {
            this.taskService.updateProject(this.projectDetail).subscribe(function (A) { _this.getAllProjectDetails; });
        }
    };
    ProjectComponent.prototype.sortByDate = function () {
        this.allProjectList.sort(function (a, b) {
            var dateA = new Date(a.Start_Date);
            var dateB = new Date(b.Start_Date);
            return dateA - dateB; //sort by date ascending
        });
    };
    ProjectComponent.prototype.sortByEnddate = function () {
        this.allProjectList.sort(function (a, b) {
            var dateA = new Date(a.End_Date);
            var dateB = new Date(b.End_Date);
            return dateA - dateB; //sort by date ascending
        });
    };
    ProjectComponent.prototype.sortByPriority = function () {
        this.allProjectList.sort(function (a, b) {
            var nameA = a.Priority, nameB = b.Priority;
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
    };
    ProjectComponent.prototype.sortByCompleted = function () {
        this.allProjectList.sort(function (a, b) {
            var nameA = a.NoOfTasksCompleted, nameB = b.NoOfTasksCompleted;
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
    };
    ProjectComponent.prototype.editProject = function (project, startandEndDate) {
        this.projectDetail.End_Date = this.datePipe.transform(project.End_Date, 'yyyy-MM-dd');
        ;
        this.projectDetail.Start_Date = this.datePipe.transform(project.Start_Date, 'yyyy-MM-dd');
        this.projectDetail.Priority = project.Priority;
        this.projectDetail.Project_Name = project.Project_Name;
        this.projectDetail.Project_ID = project.Project_ID;
        this.userDetail = this.allUserList.filter(function (A) { return A.User_ID == project.User_ID; })[0];
        this.projectDetail.User_ID = this.allUserList.filter(function (A) { return A.User_ID == project.User_ID; })[0].User_ID;
        this.mode = 'Edit';
        this.disabledControl = false;
        if (this.projectDetail.Start_Date && this.projectDetail.End_Date) {
            startandEndDate.checked = true;
        }
        document.getElementById("submitBtn").innerHTML = 'Edit';
    };
    ProjectComponent.prototype.onClear = function (startandEndDate) {
        this.projectDetail = new _models_projectModel__WEBPACK_IMPORTED_MODULE_5__["projectModel"]();
        document.getElementById("submitBtn").innerHTML = 'Add';
        this.mode = 'Add';
        startandEndDate.checked = false;
        this.disabledControl = true;
        this.userDetail = new _models_usermodel__WEBPACK_IMPORTED_MODULE_1__["userModel"]();
    };
    ProjectComponent.prototype.deleteProject = function (project) {
        var _this = this;
        this.taskService.deleteProject(project.Project_ID).subscribe(function (A) {
        }, function (error) { }, function () {
            _this.getAllProjectDetails();
        });
    };
    ProjectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project',
            template: __webpack_require__(/*! ./project.component.html */ "./src/app/project/project.component.html"),
            styles: [__webpack_require__(/*! ./project.component.css */ "./src/app/project/project.component.css")],
            providers: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"]]
        }),
        __metadata("design:paramtypes", [_services_taskservice_service__WEBPACK_IMPORTED_MODULE_2__["TaskserviceService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"]])
    ], ProjectComponent);
    return ProjectComponent;
}());



/***/ }),

/***/ "./src/app/services/taskservice.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/taskservice.service.ts ***!
  \*************************************************/
/*! exports provided: TaskserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskserviceService", function() { return TaskserviceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TaskserviceService = /** @class */ (function () {
    function TaskserviceService(httpClient) {
        this.httpClient = httpClient;
        this.appconstants = new _constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"]();
    }
    TaskserviceService.prototype.getAllTasks = function () {
        var url = this.appconstants.baseUrl + this.appconstants.getTasks;
        return this.httpClient.get(url);
    };
    TaskserviceService.prototype.getAllTasksByID = function (taskID) {
        var url = this.appconstants.baseUrl + this.appconstants.getTaskByID;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('id', taskID);
        return this.httpClient.get(url, { params: params });
    };
    TaskserviceService.prototype.addTask = function (task) {
        var url = this.appconstants.baseUrl + this.appconstants.addTask;
        return this.httpClient.post(url, task);
    };
    TaskserviceService.prototype.updateTask = function (task) {
        var url = this.appconstants.baseUrl + this.appconstants.updateTask;
        return this.httpClient.put(url, task);
    };
    //user
    TaskserviceService.prototype.adduser = function (user) {
        var url = this.appconstants.baseUrl + this.appconstants.addUser;
        return this.httpClient.post(url, user);
    };
    TaskserviceService.prototype.updateUser = function (user) {
        var url = this.appconstants.baseUrl + this.appconstants.updateUser;
        return this.httpClient.put(url, user);
    };
    TaskserviceService.prototype.getAllUsers = function () {
        var url = this.appconstants.baseUrl + this.appconstants.getAllUsers;
        return this.httpClient.get(url);
    };
    TaskserviceService.prototype.deleteuser = function (id) {
        var url = this.appconstants.baseUrl + this.appconstants.deleteUser;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('id', id);
        return this.httpClient.delete(url, { params: params });
    };
    //Project
    TaskserviceService.prototype.PostProject = function (projectdetail) {
        var url = this.appconstants.baseUrl + this.appconstants.postProject;
        return this.httpClient.post(url, projectdetail);
    };
    TaskserviceService.prototype.getAllProjectDetails = function () {
        var url = this.appconstants.baseUrl + this.appconstants.getAllProjectDetails;
        return this.httpClient.get(url);
    };
    TaskserviceService.prototype.updateProject = function (projectdetail) {
        var url = this.appconstants.baseUrl + this.appconstants.updateProject;
        return this.httpClient.put(url, projectdetail);
    };
    TaskserviceService.prototype.deleteProject = function (id) {
        var url = this.appconstants.baseUrl + this.appconstants.deleteProject;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('id', id);
        return this.httpClient.delete(url, { params: params });
    };
    TaskserviceService.prototype.getAllParentTasks = function () {
        var url = this.appconstants.baseUrl + this.appconstants.getAllParentTasks;
        return this.httpClient.get(url);
    };
    TaskserviceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], TaskserviceService);
    return TaskserviceService;
}());



/***/ }),

/***/ "./src/app/user/user.component.css":
/*!*****************************************!*\
  !*** ./src/app/user/user.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".textboxwidth{\r\n    width:370px;\r\n    font-size: 12px;\r\n    background-color:#E8E8E8;\r\n}\r\n\r\n.labelstyle{\r\n    font-weight: 15px;\r\n    font-size: 15px; \r\n    }\r\n\r\n.buttonwidth{\r\n        width:75px; \r\n        font-size: 12px;\r\n        font-weight: bold; \r\n        margin-top: 25px;\r\n    }\r\n\r\n.labeltextarea{\r\n        width:180px;\r\n        height:70px; \r\n        border: none;\r\n        background-color:#E8E8E8;\r\n        text-align: center;\r\n        font-weight: normal;\r\n        font-size: 12px;\r\n        line-height:70px;\r\n    }"

/***/ }),

/***/ "./src/app/user/user.component.html":
/*!******************************************!*\
  !*** ./src/app/user/user.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "   \n<br/><br/>\n<div class=\"row\">\n    <div class=\"col-lg-12 col-md-12\">\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            <label class=\"labelstyle\">First Name:</label>\n            </div>\n            <div class=\"col-lg-5 col-md-5 col-sm-5\">\n            <input  [(ngModel)]=\"userDetail.First_Name\"   class=\"textboxwidth\" type=\"text\" />           \n          </div>        \n  </div>\n  </div>\n  <br/>\n  <div class=\"row\">\n      <div class=\"col-lg-12 col-md-12\">\n          <div class=\"col-lg-1 col-md-1 col-sm-1\">\n              <label class=\"labelstyle\">Last Name:</label>\n              </div>               \n              <div class=\"col-lg-5 col-md-5 col-sm-5\">               \n               <input  [(ngModel)]=\"userDetail.Last_Name\"   class=\"textboxwidth\" type=\"text\" />                   \n    </div>\n  </div>\n</div>\n\n  <br/>\n  <div class=\"row\">\n      <div class=\"col-lg-12 col-md-12\">\n          <div class=\"col-lg-1 col-md-1 col-sm-1\">\n              <label class=\"labelstyle\">Employee ID:</label>\n              </div>\n              <div class=\"col-lg-5 col-md-5 col-sm-5\">\n                  <input  [(ngModel)]=\"userDetail.Employee_ID\"   class=\"textboxwidth\" type=\"number\" />   \n            </div>        \n    </div>\n  </div>\n  <br/><br/>\n  <div class=\"row\">\n      <div class=\"col-lg-12 col-md-12\">\n          <div class=\"col-lg-1 col-md-1 col-sm-1\">\n               \n              </div>\n              <div class=\"col-lg-5 col-md-5 col-sm-5\">\n                  <button #submitBtn id=\"submitBtn\" class=\"buttonwidth\" (click)=\"onSubmit()\">Add </button>&nbsp;\n                  <button id=\"clearBtn\" class=\"buttonwidth\" (click)=\"onClear()\">Reset</button>\n            </div>        \n    </div>\n  </div>\n<br/>\n  <div class=\"row\">\n      <hr style=\"border: 1px dotted gainsboro;\" />\n      <div class=\"col-lg-12 col-md-12\">\n      \n          <div class=\"col-lg-1 col-md-1 col-sm-1\">\n              <label class=\"labelstyle\">Search:</label>\n              </div>\n              <div class=\"col-lg-3 col-md-3 col-sm-3\">\n              <input  [(ngModel)]=\"searchString\"   class=\"textboxwidth\" type=\"text\" />           \n            </div> \n            \n            <div class=\"col-lg-2 col-md-2 col-sm-2\">\n                <label >Sort:</label>\n                </div>\n\n                <div class=\"col-lg-6 col-md-6 col-sm-6\">\n                    <button  (click)=\"sortFirst_Name()\" >First name</button>&nbsp;\n                    <button (click)=\"sortLast_Name()\" >last name</button>&nbsp;\n                    <button (click)=\"sortEmpid()\"  >ID</button>&nbsp;\n                    </div>\n                   \n\n      </div>\n  </div>\n\n  <!-- <div *ngFor=\"let user of allUserList |filter : 'First_Name' : searchString; let i = index\">\n      <div class=\"row\">      \n                    \n                \n          <hr style=\"border: 1px dotted gainsboro;\" />\n              \n              <div class=\"col-lg-12 col-md-12\">\n                  <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                      <label class=\"labeltextarea\">{{user.First_Name}}</label> <br/>  \n                      <label class=\"labeltextarea\">{{user.Last_Name}}</label> <br/>   \n                      <label class=\"labeltextarea\">{{user.Employee_ID}}</label> <br/>    \n                      </div>\n              \n\n              <div class=\"col-lg-8 col-md-8 col-sm-8\">\n                  <button #editButton class=\"buttonwidth\"  id=\"editButton\" (click)=\"editUser(user)\" >Edit</button>&nbsp;\n                   <button #deleteButton id=\"deleteButton\"  (click)=\"deleteUser(user)\">Delete user</button> \n                </div>             \n            </div>      \n  </div> -->\n\n  <br/><br/>\n  <div class=\"col-lg-12 col-md-12 col-sm-12\">\n  <ul class=\"list-group\" *ngFor=\"let user of allUserList |filter : 'First_Name' : searchString; let i = index\">\n      <!-- <li class=\"list-group-item d-flex justify-content-between align-items-center\"> -->\n        <div class=\"row\">\n            <hr style=\"border: 1px dotted gainsboro;\" />\n        <div class=\"col-lg-4 col-md-4 col-sm-4\">\n          <label class=\"labeltextarea\">{{user.First_Name}}</label> <br/> \n          <label class=\"labeltextarea\">{{user.Last_Name}}</label> <br/>\n          <label class=\"labeltextarea\">{{user.Employee_ID}}</label><br/>\n        </div>\n        <div class=\"col-lg-3 col-md-3 col-sm-3\">\n          <button #editButton class=\"buttonwidth\"  id=\"editButton\" (click)=\"editUser(user)\" >Edit</button> <br/><br/>\n                   <button #deleteButton id=\"deleteButton\"  (click)=\"deleteUser(user)\">Delete user</button> \n        </div>\n      </div>\n      <!-- </li> -->\n      \n    </ul>\n  </div>\n"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/*!****************************************!*\
  !*** ./src/app/user/user.component.ts ***!
  \****************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_taskservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/taskservice.service */ "./src/app/services/taskservice.service.ts");
/* harmony import */ var _models_userModel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/userModel */ "./src/app/models/userModel.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserComponent = /** @class */ (function () {
    function UserComponent(taskService, route, router, datePipe) {
        this.taskService = taskService;
        this.route = route;
        this.router = router;
        this.datePipe = datePipe;
        this.mode = "Add";
        this.disabled = false;
        this.searchString = '';
    }
    UserComponent.prototype.ngOnInit = function () {
        this.userDetail = new _models_userModel__WEBPACK_IMPORTED_MODULE_3__["userModel"]();
        this.allUserList = [];
        this.getAlluser();
    };
    UserComponent.prototype.getAlluser = function () {
        var _this = this;
        this.taskService.getAllUsers().subscribe(function (getuser) {
            _this.allUserList = getuser;
        });
    };
    UserComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.validatesubmit()) {
            if (this.mode == "Edit") {
                this.taskService.updateUser(this.userDetail)
                    .subscribe(function (result) {
                    //this.router.navigateByUrl('/ViewTaskManager');
                    _this.getAlluser();
                });
            }
            else {
                this.taskService.adduser(this.userDetail)
                    .subscribe(function (result) {
                    //this.router.navigateByUrl('/ViewTaskManager');
                    _this.getAlluser();
                });
            }
        }
        else {
            alert('Please populate all fields');
        }
    };
    UserComponent.prototype.sortFirst_Name = function () {
        this.allUserList.sort(function (a, b) {
            var nameA = a.First_Name.toLowerCase(), nameB = b.First_Name.toLowerCase();
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
    };
    UserComponent.prototype.sortLast_Name = function () {
        this.allUserList.sort(function (a, b) {
            var nameA = a.Last_Name.toLowerCase(), nameB = b.Last_Name.toLowerCase();
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
    };
    UserComponent.prototype.sortEmpid = function () {
        this.allUserList.sort(function (a, b) {
            var nameA = a.Employee_ID, nameB = b.Employee_ID;
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
    };
    UserComponent.prototype.editUser = function (user) {
        var _this = this;
        this.userDetail.First_Name = user.First_Name;
        this.userDetail.Last_Name = user.Last_Name;
        this.userDetail.Employee_ID = user.Employee_ID;
        this.userDetail.User_ID = user.User_ID;
        this.mode = 'Edit';
        if (this.validatesubmit()) {
            // $('submitBtn').innerHTML  = 'Edit';
            document.getElementById("submitBtn").innerHTML = 'Edit';
            this.taskService.updateUser(this.userDetail)
                .subscribe(function (result) {
                //this.router.navigateByUrl('/ViewTaskManager');
            }, function (error) { }, function () { _this.getAlluser(); });
        }
        else {
            alert('Please populate all fields');
        }
    };
    UserComponent.prototype.deleteUser = function (user) {
        var _this = this;
        this.taskService.deleteuser(user.User_ID).subscribe(function (A) {
        }, function (error) { }, function () {
            _this.getAlluser();
        });
    };
    UserComponent.prototype.onClear = function () {
        this.userDetail = new _models_userModel__WEBPACK_IMPORTED_MODULE_3__["userModel"]();
        document.getElementById("submitBtn").innerHTML = 'Add';
        this.mode = 'Add';
    };
    UserComponent.prototype.validatesubmit = function () {
        if (this.userDetail.First_Name && this.userDetail.Last_Name && this.userDetail.Employee_ID) {
            return true;
        }
        return false;
    };
    UserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! ./user.component.html */ "./src/app/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.css */ "./src/app/user/user.component.css")],
            providers: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]]
        }),
        __metadata("design:paramtypes", [_services_taskservice_service__WEBPACK_IMPORTED_MODULE_2__["TaskserviceService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/app/viewtask/viewtask.component.css":
/*!*************************************************!*\
  !*** ./src/app/viewtask/viewtask.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".labelstyle{\r\nfont-weight: 10px;\r\nfont-size: 13px; \r\n}\r\n\r\n.labeltextarea{\r\n    width:170px;\r\n    height:80px; \r\n    border: none;\r\n    background-color:#E8E8E8;\r\n    text-align: center;\r\n    font-weight: normal;\r\n    font-size: 12px;\r\n    line-height:70px;\r\n}\r\n\r\n.textboxwidth{\r\n    width:370px;\r\n    font-size: 12px;\r\n    background-color:#E8E8E8;\r\n}\r\n\r\n.textboxTablewidth{\r\n    width:200px;\r\n    height:70px;\r\n    border: none;\r\n    font-weight: normal; \r\n    font-size: 12px; \r\n     \r\n}\r\n\r\n.searchtextbox{\r\n    width:115px;\r\n    font-size: 12px;\r\n    background-color:#E8E8E8;\r\n}\r\n\r\n.datetextbox{\r\n    width:115px;\r\n    font-size: 12px;\r\n    background-color:#E8E8E8;\r\n}\r\n\r\n.containerclass{\r\n    border: 1px solid gainsboro;\r\n    padding-top: 25px;\r\n    padding-bottom: 25px;\r\n    \r\n}\r\n\r\n.buttonwidth{\r\n    width:75px; \r\n    font-size: 12px;\r\n    font-weight: bold; \r\n    margin-top: 25px;\r\n}\r\n\r\n.gridheader{\r\n    font-size: 12px;\r\n    font-weight: bold; \r\n}"

/***/ }),

/***/ "./src/app/viewtask/viewtask.component.html":
/*!**************************************************!*\
  !*** ./src/app/viewtask/viewtask.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br/><br/>\n<div class=\"col-lg-12 col-md-12 col-sm-12 \">\n<!-- <div class=\"row\">     \n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n      <label class=\"labelstyle\">Task:</label>\n      </div>\n      <div class=\"col-lg-5 col-md-5 col-sm-5\">\n      <input #task class=\"textboxwidth\" type=\"text\" (blur)=\"search(task,parenttask,PriorityFrom,Priorityto,StartDate,EndDate)\" />\n        </div>\n     \n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n            <label class=\"labelstyle\">Parent Task:</label>\n        </div>\n        <div class=\"col-lg-5 col-md-5 col-sm-5\">\n        <input #parenttask class=\"textboxwidth\" (blur)=\"search(task,parenttask,PriorityFrom,Priorityto,StartDate,EndDate)\" type=\"text\" />\n          </div>\n   </div>   -->\n    <div class=\"row\">     \n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n      <label class=\"labelstyle\">Project:</label>\n      </div>\n      <div class=\"col-lg-2 col-md-2 col-sm-2\" >\n     \n       <input #Project [(ngModel)]=\"this.getProject.Project_Name\"  disabled='true'   type=\"text\" />    \n        </div>\n        <div class=\"col-lg-2 col-md-2 col-sm-2\">            \n            <button id=\"SearchBtn\" data-toggle=\"modal\" data-target=\"#projectModal\" >Search</button>  \n         </div>\n     \n        <div class=\"col-lg-2 col-md-2 col-sm-2\">\n            <label >Sort Task By:</label>\n        </div>\n        <div class=\"col-lg-5 col-md-5 col-sm-5\">\n            <button (click)=\"sortByDate()\">Start Date</button> &nbsp;\n            <button (click)=\"sortByEnddate()\" >End Date</button>&nbsp;\n            <button (click)=\"sortByPriority()\" >Priority</button>&nbsp;\n            <button (click)=\"sortByCompleted()\"  >Completed</button>\n          </div>\n   </div>   \n\n\n   <br/> \n\n\n<!-- <div class=\"row\">  \n    <div class=\"col-lg-1 col-md-1 col-sm-1\">\n      <label class=\"labelstyle\">Priority From:</label>\n    </div>\n    <div class=\"col-lg-1 col-md-1 col-sm-1\">\n      <input #PriorityFrom class=\"searchtextbox\" (blur)=\"search(task,parenttask,PriorityFrom,Priorityto,StartDate,EndDate)\"  type=\"number\" />\n      </div>\n      <div class=\"col-lg-1 col-md-1 col-sm-1\">\n          <label class=\"labelstyle\">Priority To:</label>\n        </div>\n        <div class=\"col-lg-1 col-md-1 col-sm-1\">\n          <input #Priorityto  class=\"searchtextbox\" (blur)=\"search(task,parenttask,PriorityFrom,Priorityto,StartDate,EndDate)\" type=\"number\" />\n      </div>\n      <div class=\"col-lg-2 col-md-2 col-sm-2\">\n           \n      </div>\n      <div class=\"col-lg-1 col-md-1 col-sm-1\">\n          <label class=\"labelstyle\">Start Date:</label>\n      </div>\n        <div class=\"col-lg-1 col-md-1 col-sm-1\"> \n          <input #StartDate  class=\"datetextbox\" (change)=\"search(task,parenttask,PriorityFrom,Priorityto,StartDate,EndDate)\" type=\"date\" />\n      </div>\n      <div class=\"col-lg-1 col-md-1 col-sm-1\">\n          <label class=\"labelstyle\">End Date:</label>\n         </div>\n         <div class=\"col-lg-1 col-md-1 col-sm-1\">\n          <input #EndDate class=\"datetextbox\" (change)=\"search(task,parenttask,PriorityFrom,Priorityto,StartDate,EndDate)\" type=\"date\" />\n      </div>\n</div> -->\n<br/>\n<hr style=\"border: 1px dotted gainsboro;\" /> \n<div *ngFor=\"let task of filterAllTaskItems\">\n<div class=\"row\">      \n    <div class=\"col-lg-2 col-md-2 col-sm-2\">\n        <label class=\"gridheader\">Task</label><br/>\n        <label class=\"labeltextarea\">{{task.TaskName}}</label>         \n        \n        </div>\n        <div class=\"col-lg-2 col-md-2 col-sm-2\">\n            <label class=\"gridheader\">Parent</label><br/>         \n            <label class=\"labeltextarea\">{{task.Parent_Task}} </label> \n         </div>\n            <div class=\"col-lg-2 col-md-2 col-sm-2\">\n          <label class=\"gridheader\">Priority</label><br/>\n          <label class=\"textboxTablewidth\">{{task.Priority}} </label> \n      </div>\n      <div class=\"col-lg-2 col-md-2 col-sm-2\">\n\n          <label class=\"gridheader\">Start</label><br/>\n          <label class=\"textboxTablewidth\">{{task.StartDate | date:'dd/MM/yyy'}} </label> \n      </div>\n      <div class=\"col-lg-2 col-md-2 col-sm-2\">\n          <label class=\"gridheader\">End</label><br/>\n          <label class=\"textboxTablewidth\">{{task.EndDate | date:'dd/MM/yyy'}} </label> \n      </div>\n      \n      <div>\n        <button class=\"buttonwidth\" [disabled]=\"task.DisabledMode\" [routerLink]=\"['/Addupdatetask',task.Task_ID]\">Edit</button>&nbsp;\n        <button class=\"buttonwidth\" (click)=\"EndTask(task)\">End Task</button>\n      </div>\n        \n</div>\n\n</div>\n\n</div>\n\n<div class=\"modal fade\" id=\"projectModal\" #projectModal tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Project</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          \n            <div class=\"row\">\n                <hr style=\"border: 1px dotted gainsboro;\" />\n                <div class=\"col-lg-12 col-md-12\">\n                \n                    <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label class=\"labelstyle\">Search by Name:</label>\n                        </div>\n                        <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                        <input  [(ngModel)]=\"searchString\"   class=\"textboxwidth\" type=\"text\" />           \n                      </div>\n                      \n                </div>\n            </div>\n          \n                      \n            <br/><br/>\n            <div class=\"col-lg-12 col-md-12 col-sm-12\">\n            <ul class=\"list-group\" *ngFor=\"let project of allProjectList |filter : 'Project_Name' : searchString; let i = index\">\n               \n                  <div class=\"row\">\n                      <hr style=\"border: 1px dotted gainsboro;\" />\n                  <div class=\"col-lg-4 col-md-4 col-sm-4\">\n                        <label > Project Name: </label>\n                    <label >{{project.Project_Name}}</label> <br/> \n                    <label > Start Date: </label>\n                    <label >{{project.Start_Date}}</label> <br/>\n                    <label > End_Date: </label>\n                    <label >{{project.End_Date}}</label><br/>\n                  </div>\n                  <div class=\"col-lg-3 col-md-3 col-sm-3\">\n                    <button #selectButton class=\"buttonwidth\"  id=\"selectButton\" (click)=\"selectproject(project,projectModal)\" >Select</button> <br/><br/>\n                            \n                  </div>\n                </div>\n                                \n              </ul>\n            </div>\n          \n\n\n\n        </div>\n        <div class=\"modal-footer\">\n          <!-- <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n        </div>\n      </div>\n    </div>\n  </div> <!--End of Project-->\n"

/***/ }),

/***/ "./src/app/viewtask/viewtask.component.ts":
/*!************************************************!*\
  !*** ./src/app/viewtask/viewtask.component.ts ***!
  \************************************************/
/*! exports provided: ViewtaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewtaskComponent", function() { return ViewtaskComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_taskmodel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/taskmodel */ "./src/app/models/taskmodel.ts");
/* harmony import */ var _services_taskservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/taskservice.service */ "./src/app/services/taskservice.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _models_projectModel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/projectModel */ "./src/app/models/projectModel.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ViewtaskComponent = /** @class */ (function () {
    function ViewtaskComponent(taskService, datePipe) {
        this.taskService = taskService;
        this.datePipe = datePipe;
        this.allTaskItems = [];
        this.filterAllTaskItems = [];
        this.allProjectList = [];
        this.allParenttask = [];
    }
    ViewtaskComponent.prototype.ngOnInit = function () {
        this.selectedproject = new _models_projectModel__WEBPACK_IMPORTED_MODULE_4__["projectModel"]();
        this.getProject = new _models_projectModel__WEBPACK_IMPORTED_MODULE_4__["projectModel"]();
        this.addTask = this.setaddTask();
        this.viewTask = this.setviewTask();
        //this.addTask.innerHTML="Add Task";
        //this.viewTask.innerHTML="View Task"; 
        this.getAllTasks();
        this.getAllProjectDetails();
    };
    ViewtaskComponent.prototype.setaddTask = function () {
        // document.getElementById("user").innerHTML=true;
        return document.getElementById("updateAnchor");
    };
    ViewtaskComponent.prototype.setviewTask = function () {
        return document.getElementById("viewAnchor");
    };
    ViewtaskComponent.prototype.getAllProjectDetails = function () {
        var _this = this;
        this.taskService.getAllProjectDetails().subscribe(function (getProject) {
            _this.allProjectList = getProject;
        });
    };
    ViewtaskComponent.prototype.getAllTasks = function () {
        var _this = this;
        this.allTaskItems = [];
        this.filterAllTaskItems = [];
        this.taskService.getAllTasks()
            .subscribe(function (result) {
            _this.taskItems = result;
            //this.allTaskItems=this.taskItems;
            _this.taskItems.forEach(function (taskModelValue) {
                var gettaskModel = new _models_taskmodel__WEBPACK_IMPORTED_MODULE_1__["taskModel"]();
                gettaskModel.Priority = taskModelValue.Priority;
                gettaskModel.EndDate = taskModelValue.EndDate;
                gettaskModel.StartDate = taskModelValue.StartDate;
                gettaskModel.Task_ID = taskModelValue.Task_ID;
                gettaskModel.Parent_ID = taskModelValue.Parent_ID;
                gettaskModel.TaskName = taskModelValue.TaskName;
                gettaskModel.Mode = taskModelValue.Mode;
                gettaskModel.Project_ID = taskModelValue.Project_ID;
                gettaskModel.Project_Name = taskModelValue.Project_Name;
                if (_this.allProjectList.filter(function (A) { return A.Project_ID == taskModelValue.Project_ID; })) {
                    _this.selectedproject = _this.allProjectList.filter(function (A) { return A.Project_ID == taskModelValue.Project_ID; })[0];
                }
                if (_this.selectedproject) {
                    gettaskModel.Project_Name = _this.selectedproject.Project_Name;
                }
                if (taskModelValue.Status == "Y") {
                    gettaskModel.DisabledMode = true;
                }
                else {
                    gettaskModel.DisabledMode = false;
                }
                _this.allTaskItems.push(gettaskModel);
                _this.filterAllTaskItems.push(gettaskModel);
            });
            _this.getAllParentTask();
        });
    };
    ViewtaskComponent.prototype.EndTask = function (value) {
        var _this = this;
        value.Status = "Y";
        this.taskService.updateTask(value)
            .subscribe(function (result) {
            _this.getAllTasks();
        });
    };
    /*search(task:any,parenttask:any,PriorityFrom:any,Priorityto:any,StartDate:any,EndDate:any):void
    {
      let filteredTaskItems:taskModel[]=this.allTaskItems;
     
      if(task.value)
      {
        filteredTaskItems=filteredTaskItems.filter(A=>A.TaskName.toLocaleUpperCase().includes(task.value.toString().toLocaleUpperCase())) ;
      }
      if(parenttask.value)
      {
        filteredTaskItems=filteredTaskItems.filter(A=>A.Parent_ID==parenttask.value);
      }
      if(Priorityto.value)
      {
        filteredTaskItems=filteredTaskItems.filter(A=>parseInt(A.Priority)<=parseInt(Priorityto.value))
      }
      if(PriorityFrom.value)
      {
        filteredTaskItems=filteredTaskItems.filter(A=>parseInt(A.Priority)>=parseInt(PriorityFrom.value))
      }
      
      if(StartDate.value)
      {
        
  
        filteredTaskItems=filteredTaskItems.filter(A=> Date.parse(A.StartDate)>=Date.parse(StartDate.value))
      }
      
      if(EndDate.value)
      {
        filteredTaskItems=filteredTaskItems.filter(A=> Date.parse(A.EndDate)<=Date.parse(EndDate.value))
      }
      
      this.filterAllTaskItems=filteredTaskItems;
    }*/
    ViewtaskComponent.prototype.selectproject = function (project, userModal) {
        this.getProject = project;
        var filteredTaskItems = this.allTaskItems;
        if (project.Project_Name) {
            filteredTaskItems = filteredTaskItems.filter(function (A) { return A.Project_ID == project.Project_ID.toString(); });
        }
        this.filterAllTaskItems = filteredTaskItems;
    };
    ViewtaskComponent.prototype.getdateValue = function (value) {
        var pipedValue = this.datePipe.transform(value, 'dd/MM/yyyy');
        return pipedValue;
    };
    ViewtaskComponent.prototype.sortByDate = function () {
        this.filterAllTaskItems.sort(function (a, b) {
            var dateA = new Date(a.StartDate);
            var dateB = new Date(b.StartDate);
            return dateA - dateB; //sort by date ascending
        });
    };
    ViewtaskComponent.prototype.sortByEnddate = function () {
        this.filterAllTaskItems.sort(function (a, b) {
            var dateA = new Date(a.EndDate);
            var dateB = new Date(b.EndDate);
            return dateA - dateB; //sort by date ascending
        });
    };
    ViewtaskComponent.prototype.sortByPriority = function () {
        this.filterAllTaskItems.sort(function (a, b) {
            var nameA = a.Priority, nameB = b.Priority;
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
    };
    ViewtaskComponent.prototype.sortByCompleted = function () {
        this.filterAllTaskItems.sort(function (a, b) {
            var nameA = a.Status, nameB = b.Status;
            if (nameA < nameB) //sort string ascending
                return -1;
            if (nameA > nameB)
                return 1;
            return 0; //default return value (no sorting)
        });
    };
    ViewtaskComponent.prototype.getAllParentTask = function () {
        var _this = this;
        this.taskService.getAllParentTasks().subscribe(function (getParenttask) {
            _this.allParenttask = getParenttask;
            /*if(this.taskDetail.Parent_ID!=null)
            this.getparentTask=this.allParenttask.filter(A=>A.Parent_ID ==this.taskDetail.Parent_ID)[0];*/
            _this.filterAllTaskItems.forEach(function (taskitems) {
                if (_this.allParenttask.some(function (A) { return A.Parent_ID == taskitems.Parent_ID; })) {
                    taskitems.Parent_Task = _this.allParenttask.filter(function (A) { return A.Parent_ID == taskitems.Parent_ID; })[0].Parent_Task;
                }
            });
        });
    };
    ViewtaskComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-viewtask',
            template: __webpack_require__(/*! ./viewtask.component.html */ "./src/app/viewtask/viewtask.component.html"),
            styles: [__webpack_require__(/*! ./viewtask.component.css */ "./src/app/viewtask/viewtask.component.css")],
            providers: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"]]
        }),
        __metadata("design:paramtypes", [_services_taskservice_service__WEBPACK_IMPORTED_MODULE_2__["TaskserviceService"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"]])
    ], ViewtaskComponent);
    return ViewtaskComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Gautam\FinalProject\FinalProject_FSD\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map