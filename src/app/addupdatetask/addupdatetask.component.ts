import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskserviceService } from '../services/taskservice.service';
import { taskModel } from '../models/taskmodel';
import { DatePipe } from '@angular/common';
import { projectModel } from '../models/projectModel';
import { userModel } from '../models/usermodel';
import {parentTaskModel} from '../models/parentTaskModel'
import * as $ from 'jquery';
import { error } from 'protractor';

@Component({
  selector: 'app-addupdatetask',
  templateUrl: './addupdatetask.component.html',
  styleUrls: ['./addupdatetask.component.css'],
  providers: [DatePipe]
})
export class AddupdatetaskComponent implements OnInit {

  taskID:string;
  taskDetail:taskModel;
  mode:string="Add";
  disabled:boolean=false;
  getProject:projectModel;
  allProjectList :projectModel[]=[];
  userDetail:userModel;
  allUserList:userModel[];
  getparentTask:parentTaskModel;
  allParenttask :parentTaskModel[]=[];
  accesslevel:boolean =false;

  constructor(private taskService:TaskserviceService, private route:ActivatedRoute,private router:Router,private datePipe: DatePipe) { }

  ngOnInit() {   
    this.taskID = this.route.snapshot.paramMap.get('id');
    this.taskDetail=new taskModel();
    this.getProject =   new projectModel();
    this.userDetail = new userModel(); 
    this.getparentTask= new parentTaskModel();
    this.allUserList = [];
    debugger;
    if(this.mode=="Add")
    {
     this.SelectDate();
    this.getAllProjectDetails();
    this.getAlluser();
    this.getAllParentTask();
    }

    if(this.taskID)
    {
      let addTask= document.getElementById("updateAnchor");
      let viewTask= document.getElementById("viewAnchor");
      let submitButton= document.getElementById("submitBtn");
      let cancelButton= document.getElementById("clearBtn");
      //this.SelectDate();
      addTask.innerHTML="Update Task";
      //viewTask.innerHTML="";
      submitButton.innerHTML="Update";
      cancelButton.innerHTML="Cancel";
      this.mode="Edit";
      this.getTaskByID();
       
    }
  }
  getAllProjectDetails()
  {   

    this.taskService.getAllProjectDetails().subscribe(
      getProject=>{        
        this.allProjectList = getProject;

        //this.getProject 
        if(this.taskDetail.Project_ID!=null)
        this.getProject=this.allProjectList.filter(A=>A.Project_ID ==this.taskDetail.Project_ID)[0];
      
      })
  }

  getAllParentTask()
  {   

    this.taskService.getAllParentTasks().subscribe(
      getParenttask=>{        
        this.allParenttask = getParenttask;
        if(this.taskDetail.Parent_ID!=null)
        this.getparentTask=this.allParenttask.filter(A=>A.Parent_ID ==this.taskDetail.Parent_ID)[0];
      })
  }

  getAlluser()
  {
    this.taskService.getAllUsers().subscribe(
      getuser=>{ 
        debugger;       
        this.allUserList = getuser;
        if(this.taskDetail.User_ID!=null)
        this.userDetail=this.allUserList.filter(A=>A.User_ID ==this.taskDetail.User_ID)[0];
      })
         
  }

  getTaskByID()
  {    
    this.taskService.getAllTasksByID(this.taskID)
    .subscribe(result=>{  
      this.taskDetail=<taskModel>result;
    

      this.taskDetail.StartDate=this.datePipe.transform(this.taskDetail.StartDate, 'yyyy-MM-dd');
      this.taskDetail.EndDate=this.datePipe.transform(this.taskDetail.EndDate, 'yyyy-MM-dd');       
      if(this.taskDetail.Status=="Y")
      {
        //this.disabled=true;
        this.taskDetail.StartDate=null;
      this.taskDetail.EndDate=null; 
      }
      else{
        this.disabled=false;
        this.taskDetail.StartDate=this.datePipe.transform(this.taskDetail.StartDate, 'yyyy-MM-dd');
      this.taskDetail.EndDate=this.datePipe.transform(this.taskDetail.EndDate, 'yyyy-MM-dd');
      
      }
    },
  error=>{},
  ()=>{
    this.getAllProjectDetails();
    this.getAlluser();
    this.getAllParentTask();
  }
)
  }

  onSubmit(parenttask:any)
  {
    if(!this.validateMandatoryData(parenttask))
    {
      if(parenttask.checked==true)
      {
        alert('Please poulate Task Name & project Name ')
        return;
      }
      if(parenttask.checked==false)
      {
        alert('Please poulate all fields except Parent Task')
        return;
      }
    }

    if(parenttask.checked==false)
    {
      if(!this.validateDateCompare(parenttask))
      {
        alert('End Date should not be before Start date')
        return;
      }
    }

    if(parenttask.checked==true)
    {
      this.taskDetail.IsParent_Task ="Y"
    }
    else
    {
      this.taskDetail.IsParent_Task ="N"
    }

    this.taskDetail.Parent_ID  = this.getparentTask.Parent_ID  
    this.taskDetail.Project_ID  = this.getProject.Project_ID
    this.taskDetail.User_ID =  this.userDetail.User_ID;
    
    if(this.mode=="Edit")
    {
      this.taskService.updateTask(this.taskDetail)
      .subscribe(result=>{
        this.router.navigateByUrl('/ViewTaskManager');
      })
    }
    else{
      this.taskService.addTask(this.taskDetail)
      .subscribe(result=>{
        this.router.navigateByUrl('/ViewTaskManager');
      })
       
    }
  }

validateDateCompare(parentTask:any):boolean
{
  if(parentTask.checked==false )
  {
  if(new Date(this.taskDetail.StartDate) > new Date(this.taskDetail.EndDate))
  {
    return false;
  }
  //return false;
  }
  return true;
}

  SelectDate():any
  {    
    // this.disabledControl =false;
    let date = new Date();
    this.taskDetail.StartDate = this.datePipe.transform(date, 'yyyy-MM-dd');
    this.taskDetail.EndDate = this.datePipe.transform(this.addDays(date,1), 'yyyy-MM-dd');      
    
  }

  SetParenttask(parenttask:any):void
  {
    if(parenttask.checked == true) 
    {
      this.accesslevel = true;
      this.taskDetail.StartDate = null;
      this.taskDetail.EndDate = null;
    }
    else{
      this.accesslevel = false;
     this.SelectDate();
    }
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
}
  
  onClear(parenttask:any)
  {    
    
    if(parenttask.checked==true)
    {

    }
    if(this.mode=="Edit")
    {
       this.router.navigateByUrl('/ViewTaskManager');
    }
    else{
      this.taskDetail=new taskModel();
    }
  }

  onDateSelect(value:any)
  {    
    var startdate=document.getElementById("startdate");
    this.taskDetail.StartDate=startdate.innerText;

  }

  selectproject(project:projectModel,projectModal:any)
  {
    this.getProject = project;
  }

  selectparentTask(projectTask:parentTaskModel,projectTaskModal:any)
  {
    this.getparentTask = projectTask;
       
  }

  selectUser(user:userModel,userModal:any)
  {
    this.userDetail = user;
   }

validateMandatoryData(parentTask:any):boolean
{  
  debugger;
  if(parentTask.checked==true )
  {
  
    if(this.getProject.Project_Name && this.taskDetail.TaskName)
    {
    return true;
    }
    else
    {
      return false;
    }
  }  

else
{
  if(this.getProject.Project_Name && this.userDetail.User_ID && this.taskDetail.TaskName)
    {
    return true;
    }
}
  return false;   
}

}
