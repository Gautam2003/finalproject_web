export class taskModel{

    public Task_ID:string;
    public Parent_ID:string;
    public TaskName:string;
    public Priority:string;
    public StartDate:string;
    public EndDate:string;
    public Mode:string;
    public Status:string;
    public DisabledMode:boolean=false;
    public Project_Name:string;
    public Project_ID:string;
    public User_ID:string;
    public Parent_Task:string;
    public IsParent_Task:string;

    constructor()
    {
        this.Priority="0";
    }
}

