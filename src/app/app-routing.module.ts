import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewtaskComponent } from './viewtask/viewtask.component';
import { AddupdatetaskComponent } from './addupdatetask/addupdatetask.component';
import { ProjectComponent } from './project/project.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  { path: '', component: ViewtaskComponent }, 
  { path: 'ViewTaskManager', component: ViewtaskComponent },
  { path: 'Addupdatetask', component: AddupdatetaskComponent },
  { path: 'Addupdatetask/:id', component: AddupdatetaskComponent }, 
  {path:'project', component:ProjectComponent},
  {path:'user', component:UserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
