import { TestBed } from '@angular/core/testing';

import { TaskserviceService } from './taskservice.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TaskserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule, HttpClientTestingModule
  ],
  providers: [TaskserviceService]
  }));

  it('should be created', () => {
    const service: TaskserviceService = TestBed.get(TaskserviceService);
    expect(service).toBeTruthy();
  });
});

