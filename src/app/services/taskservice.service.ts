import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { AppConstants } from '../constants';
import { taskModel } from '../models/taskmodel';
import { userModel } from '../models/usermodel';
import {projectModel} from '../models/projectModel'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskserviceService {
  appconstants:AppConstants=new AppConstants();
  constructor(private httpClient:HttpClient) { }

  getAllTasks()
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.getTasks;
    return this.httpClient.get(url);
  }

  getAllTasksByID(taskID:string)
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.getTaskByID;
    let params=new HttpParams().set('id',taskID);
    return this.httpClient.get(url,{params:params});
  }

  addTask(task:taskModel)
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.addTask;     
    return this.httpClient.post(url,task);    
  }

  updateTask(task:taskModel)
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.updateTask;     
    return this.httpClient.put(url,task);  
  }

  //user

  adduser(user:userModel)
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.addUser;     
    return this.httpClient.post(url,user);    
  }

  updateUser(user:userModel)
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.updateUser;     
    return this.httpClient.put(url,user);  
  }

  getAllUsers():Observable<any>
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.getAllUsers;
    return this.httpClient.get(url);
  }

  deleteuser(id:string):Observable<any>
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.deleteUser;
    let params=new HttpParams().set('id',id);
    return this.httpClient.delete(url,{params:params});
  }

  //Project

  
  PostProject(projectdetail:projectModel):Observable<any>
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.postProject;     
    return this.httpClient.post(url,projectdetail);    
  }

  

  getAllProjectDetails():Observable<any>
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.getAllProjectDetails;
    return this.httpClient.get(url);
  }

  updateProject(projectdetail:projectModel):Observable<any>
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.updateProject;     
    return this.httpClient.put(url,projectdetail);    
  }

  deleteProject(id:string):Observable<any>
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.deleteProject;
    let params=new HttpParams().set('id',id);
    return this.httpClient.delete(url,{params:params});
  }


  getAllParentTasks():Observable<any>
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.getAllParentTasks;
    return this.httpClient.get(url);
  }
  

  

  





}
