import { Component, OnInit } from '@angular/core';
import { userModel } from '../models/usermodel';
import { TaskserviceService } from '../services/taskservice.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { projectModel } from '../models/projectModel';
import * as $ from 'jquery';


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
  providers: [DatePipe]
})
export class ProjectComponent implements OnInit {
  userDetail:userModel;
  allUserList:userModel[]; 
  allProjectList :projectModel[];
  projectDetail:projectModel;
  mode='Add';
  disabledControl:boolean =true;
  constructor(private taskService:TaskserviceService, private route:ActivatedRoute,private router:Router,private datePipe: DatePipe) { }

  ngOnInit() {
    this.userDetail=new userModel();
    this.projectDetail = new projectModel();
    this.allUserList = [];
    this.getAlluser();
    this.getAllProjectDetails();
   
  
  }

  getAlluser()
  {
    this.taskService.getAllUsers().subscribe(
      getuser=>{        
        this.allUserList = getuser;
      })
         
  }

  getAllProjectDetails()
  {   

    this.taskService.getAllProjectDetails().subscribe(
      getProject=>{        
        this.allProjectList = getProject;
      })
  }

  selectUser(user:userModel,userModal:any)
  {
    this.userDetail = user;
    //userModal.modal('hide')
    $('#userModal').modal('hide')

    //document.getElementById('userModal').hidden = true;
  }

  SelectDate(startandEndDate:any):any
  {    
    //alert(this.projectDetail.End_Date);
    if(startandEndDate.checked==true)
    {
      this.disabledControl =false;
    let date = new Date();
    this.projectDetail.Start_Date = this.datePipe.transform(date, 'yyyy-MM-dd');
    this.projectDetail.End_Date = this.datePipe.transform(this.addDays(date,1), 'yyyy-MM-dd');
    }
    else
    {
    this.projectDetail.Start_Date = null;
    this.projectDetail.End_Date = null;
    this.disabledControl =true;
    }
   
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
}

validateDateCompare(startandEndDate:any):boolean
{
  if(startandEndDate.checked==true )
  {
  if(new Date(this.projectDetail.Start_Date) > new Date(this.projectDetail.End_Date))
  {
    return false;
  }
  //return false;
  }
  return true;
}

validateDate(startandEndDate:any):boolean
{  
  if(startandEndDate.checked==true )
  {
  if(this.projectDetail.Start_Date && this.projectDetail.End_Date )
  {
    if(this.projectDetail.Project_Name && this.userDetail.User_ID)
    {
    return true;
    }
    else
    {
      return false;
    }
  }  
}
else
{
  if(this.projectDetail.Project_Name && this.projectDetail.User_ID)
    {
    return true;
    }
}
  return false;   
}

onSubmit(startandEndDate:any):void
{  
 //this.projectDetail
 debugger;
 if(!this.validateDate(startandEndDate))
 {
  alert('Please populate all fields')
  return ;
 }

 if(!this.validateDateCompare(startandEndDate))
 {
   alert('End Date should not be before Start date')
   return;
 }

 if(this.mode == 'Add')
 {
 this.projectDetail.User_ID = this.userDetail.User_ID;
 this.taskService.PostProject(this.projectDetail).subscribe(A=>{this.getAllProjectDetails},error=>{},
()=>{
  this.getAllProjectDetails();
});
 }
 else if(this.mode == 'Edit')
 {
  this.taskService.updateProject(this.projectDetail).subscribe(A=>{this.getAllProjectDetails});
 }
}


sortByDate():any
{
  this.allProjectList.sort(function(a, b){
    let dateA:any=new Date(a.Start_Date) 
    let dateB:any=new Date(b.Start_Date)
    return dateA-dateB //sort by date ascending
})
}

sortByEnddate():any
{
  this.allProjectList.sort(function(a, b){
    let dateA:any=new Date(a.End_Date) 
    let dateB:any=new Date(b.End_Date)
    return dateA-dateB //sort by date ascending
})
}

sortByPriority():any
{
  this.allProjectList.sort(function(a, b){
    var nameA=a.Priority, nameB=b.Priority
    if (nameA < nameB) //sort string ascending
        return -1 
    if (nameA > nameB)
        return 1
    return 0 //default return value (no sorting)
})
}

sortByCompleted():any
{
  this.allProjectList.sort(function(a, b){
    var nameA=a.NoOfTasksCompleted, nameB=b.NoOfTasksCompleted
    if (nameA < nameB) //sort string ascending
        return -1 
    if (nameA > nameB)
        return 1
    return 0 //default return value (no sorting)
})
}

editProject(project:projectModel,startandEndDate:any)
{  
  this.projectDetail.End_Date = this.datePipe.transform(project.End_Date, 'yyyy-MM-dd');;
    this.projectDetail.Start_Date = this.datePipe.transform(project.Start_Date, 'yyyy-MM-dd');
    this.projectDetail.Priority = project.Priority;
    this.projectDetail.Project_Name = project.Project_Name;
    this.projectDetail.Project_ID = project.Project_ID;    
    this.userDetail =   this.allUserList.filter(A=>A.User_ID==project.User_ID)[0] ;
    this.projectDetail.User_ID =  this.allUserList.filter(A=>A.User_ID==project.User_ID)[0].User_ID;
    this.mode='Edit';
    this.disabledControl = false;
    if(this.projectDetail.Start_Date && this.projectDetail.End_Date)
    {
    startandEndDate.checked = true; 
    }
    document.getElementById("submitBtn").innerHTML =  'Edit';
}

onClear(startandEndDate:any)
  {  
    
      this.projectDetail=new projectModel();  
      document.getElementById("submitBtn").innerHTML =  'Add';
      this.mode='Add';
      startandEndDate.checked = false; 
      this.disabledControl =true;
      this.userDetail = new userModel();
    
  }

  deleteProject(project:projectModel)
  {   
  this.taskService.deleteProject(project.Project_ID).subscribe(A=>{
  },
  error=>{},
  ()=>{
    this.getAllProjectDetails();
  }

  );

  }



  

}
